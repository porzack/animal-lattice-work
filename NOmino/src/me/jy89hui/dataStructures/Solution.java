package me.jy89hui.dataStructures;

public class Solution {
	private int order; 
	private byte[][] data;
	int id = 0;
	public Solution(int order, byte[][] data){
		this.order=order;
		this.data=data;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public byte[][] getData() {
		return data;
	}
	public void setData(byte[][] data) {
		this.data = data;
	}
	public String toString(){
		int h=data[0].length;
		int w=data.length;
		String res="";
		for(int y=0; y<w; y++){
			for (int x=0; x<h; x++){
				res+=data[y][x]==1?"X":" ";
			}
			res+="\n";
		}
		return res;
	}
	public String toStringBorders(){
		int h=data[0].length;
		int w=data.length;
		String res="+";
		for(int k=0; k<h;k++){
			res+="-";
		}
		res+="+\n";
		for(int y=0; y<w; y++){
			res+="|";
			for (int x=0; x<h; x++){
				res+=data[y][x]==1?"X":" ";
			}
			res+="|\n";
		}
		res+="+";
		for(int k=0; k<h;k++){
			res+="-";
		}
		res+="+";
		return res;
	}
	public boolean equalsOther(Solution other){
		if (other.order!=this.order){
			return false;
		}
		int h = this.data.length;
		int w = this.data[0].length;
		int ho = other.data.length;
		int wo = other.data[0].length;
		if (h!=ho || w!=wo){
			return false;
		}
		for (int y=0; y<h;y++){
			for (int x=0; x<w; x++){
				if (this.data[y][x] != other.data[y][x]){
					return false;
				}
			}
		}
		return true;
		
	}
	public boolean isRotationOf(Solution other){
		boolean isUnique=true;
		isUnique=equalsOther(other)?false:isUnique;
		rotateData90();
		isUnique=equalsOther(other)?false:isUnique;
		rotateData90();
		isUnique=equalsOther(other)?false:isUnique;
		rotateData90();
		isUnique=equalsOther(other)?false:isUnique;
		rotateData90();
		return !isUnique;
	}
	public boolean isRotationOrFlipOf(Solution other){
		boolean isUnique=true;
		
		isUnique=isFlipOf(other)?false:isUnique;
		rotateData90();
		isUnique=isFlipOf(other)?false:isUnique;
		rotateData90();
		isUnique=isFlipOf(other)?false:isUnique;
		rotateData90();
		isUnique=isFlipOf(other)?false:isUnique;
		rotateData90();
		return !isUnique;
	}
	private boolean isFlipOf(Solution other){
		boolean isUnique=true;
		isUnique=equalsOther(other)?false:isUnique;
		flipX();
		isUnique=equalsOther(other)?false:isUnique;
		flipX();
		flipY();
		isUnique=equalsOther(other)?false:isUnique;
		flipY();
		return !isUnique;
	}
	public void rotateData90() {
		int h = this.data.length;
		int w = this.data[0].length;
		byte[][] datanew = new byte[w][h];
		for (int x=0; x<w;x++){
			for (int y=0; y<h; y++){
				datanew[x][y] = data[h-y-1][x] ;
			}
		}
		this.data=datanew;
	}	
	public void flipX(){
		int h = this.data.length;
		int w = this.data[0].length;
		byte[][] datanew = new byte[h][w];
		for (int x=0; x<w;x++){
			for (int y=0; y<h; y++){
				datanew[y][x] = data[y][w-x-1] ;
			}
		}
		this.data=datanew;
	}
	public void flipY(){
		int h = this.data.length;
		int w = this.data[0].length;
		byte[][] datanew = new byte[h][w];
		for (int x=0; x<w;x++){
			for (int y=0; y<h; y++){
				datanew[y][x] = data[h-y-1][x];
			}
		}
		this.data=datanew;
	}
}
