package me.jy89hui.main;

import java.util.ArrayList;
import java.util.List;

import me.jy89hui.dataStructures.Solution;

public class SolutionCreator {
	boolean shouldlog=true;
	private int repetitions=1;
	private int threads=4;
	public SolutionCreator(){
		
	}
	public SolutionCreator(int threads, int repetitions){
		this.threads=threads;
		this.repetitions=repetitions;
	}
	public void setThreads(int threads){
		this.threads=threads;
	}
	public void setRepetitions(int repetitions){
		this.repetitions=repetitions;
	}
	/**
	 * ----------------------------GENERAL ------------------------------
	 */
	public void log(String s){
		if (shouldlog){
			System.out.println("[l]"+s);
		}
	}
	/**
	 * 0 = NON REMOVED DUPLICATE LIST
	 * 1 = FIXED
	 * 2 = ONE SIDED
	 * 3 = FREE
	 */
	public List<Solution> createOminos(int order, int type, boolean reportTime, int threshold){
		shouldlog=reportTime;
		long start = System.nanoTime();
		List<Solution> list = createFIXED(1, null,threshold);
		for (int x=1; x<order+1; x++){
			if (type==0){
				list = createALL(x, list);
			}else if (type==1){
				list = createFIXED(x, list,threshold);
			}else if (type==2){
				list = createONESIDED(x, list,threshold);
			}else if (type==3){
				list = createFREE(x, list,threshold);
			}
		}
		log("Order: "+order+" Type:"+type+" Seconds to complete: "+(((double)System.nanoTime()-start)/(double)(1000000000))+" items created: "+list.size());
		return list;
	}
	
	/**
	 * ----------------------------CREATE TOOLS ------------------------------
	 */
	
	public List<Solution> createFREE(int order, List<Solution> prev, int threshold){
		if (order>threshold){
			
			return multithreadEliminateDuplicates(threads,repetitions,3,createFIXED(order,prev,threshold));
		}else{
			return eliminateDuplicates(3,createALL(order,prev));
		}
	}
	public List<Solution> createONESIDED(int order, List<Solution> prev, int threshold){
		if (order>threshold){
			return multithreadEliminateDuplicates(threads,repetitions,2,createFIXED(order,prev,threshold));
		}else{
			return eliminateDuplicates(2,createALL(order,prev));
		}
	}
	public List<Solution> createFIXED(int order, List<Solution> prev, int threshold){
		if (order>threshold){
			return multithreadEliminateDuplicates(threads,repetitions,1,createALL(order,prev));
		}else{
			return eliminateDuplicates(1,createALL(order,prev));
		}
	}
	public List<Solution> createALL(int order, List<Solution> prev){
		List<Solution> slist = new ArrayList<Solution>();
		if (order==1||prev==null){
			slist.add(new Solution(1,new byte[][]{{1}}));
			return slist;
		}
		for (Solution u:prev){
			byte[][] uarray=u.getData();
			int h = uarray.length;
			int w = uarray[0].length;
			byte[][] largearray = new byte[h+2][w+2];
			insertArray(largearray, uarray);
			for (int y=0; y<h+2;y++){
				for (int x=0; x<w+2; x++){
					if (hasNeighbor(x,y,largearray)&&largearray[y][x]!=1){
						byte[][] newSolution = cloneArray(largearray);
						newSolution[y][x]=1;
						newSolution = shrink(newSolution);
						slist.add(new Solution(order,newSolution));
					}
				}
			}
		}
		return slist;
	}
	/**
	 * ----------------------------ARRAY TOOLS ------------------------------
	 */
	private int getVal(int x, int y, byte[][] data){
		try{
			return data[y][x];
		}catch (IndexOutOfBoundsException e){
			return 0;
		}
	}
	private boolean hasNeighbor(int x, int y, byte[][] data){
		return (getVal(x+1,y,data) +
				getVal(x-1,y,data) +
				getVal(x,y+1,data) +
				getVal(x,y-1,data))>0;
	}
	private byte[][] insertArray(byte[][] newa, byte[][] olda){
		int h = olda.length;
		int w = olda[0].length;
		for(int y=0; y<h; y++){
			for (int x=0; x<w; x++){
				newa[y+1][x+1]=olda[y][x];
			}
		}
		return newa;
	}
	private byte[][] cloneArray(byte[][] olda){
		int h = olda.length;
		int w = olda[0].length;
		byte[][] newa = new byte[h][w];
		for(int y=0; y<h; y++){
			for (int x=0; x<w; x++){
				newa[y][x]=olda[y][x];
			}
		}
		return newa;
	}
	private byte[][] shrink(byte[][] old){
		int minx=1000; 
		int miny=1000;
		int maxx=0;
		int maxy=0;
		int h = old.length;
		int w = old[0].length;
		for (int y=0; y<h; y++){
			for (int x=0; x<w; x++){
				if (old[y][x]==1){
					minx = x<minx?x:minx;
					miny = y<miny?y:miny;
					maxx = x>maxx?x:maxx;
					maxy = y>maxy?y:maxy;
				}
			}
		}
		int newh=maxy-miny+1;
		int neww=maxx-minx+1;
		byte[][] newa = new byte[newh][neww];
		for (int y=0; y<newh; y++){
			for (int x=0; x<neww;x++){
				newa[y][x] = old[y+miny][x+minx];
			}
		}
		return newa;
	}
	private List<Solution> shuffleArray(List<Solution> solutions){
		int length = solutions.size();
		Solution[] shuffled = new Solution[length];
		int min=0; 
		int max=length-1;
		for(int i=0; i<length; i++){
			Solution val = solutions.get(i);
			int pos;
			if (i%2==0){
				pos=min++;
			}else{
				pos=max--;
			}
			shuffled[pos]=val;
		}
		List<Solution> toReturn = new ArrayList<Solution>();
		for(Solution s:shuffled){
			if(s!=null){
				toReturn.add(s);
			}
		}
		return toReturn;
	}
	/**
	 * ----------------------------ELIMINATE DUPLICATES ------------------------------
	 */
	public List<Solution> eliminateDuplicatesComplicated(int type, List<Solution> minorList, int start, List<Solution> majorList){
		int length=minorList.size();
		minorList=eliminateDuplicates(type, minorList);
		List<Solution> uniqueList = minorList;
		for (Solution s : minorList){
			boolean unique=true;
			int pos=0;
			for (Solution us:majorList){
				if (type==1&&s.equalsOther(us)){
					unique=false;
					break;
				}
				else if (type==2&&s.isRotationOf(us)){
					unique=false;
					break;
				}
				else if (type==3&&s.isRotationOrFlipOf(us)){
					unique=false;
					break;
				}
				pos++;
			}
			if (unique){
				uniqueList.add(s);
			}
			unique=true;
		}
		return uniqueList;	
	}
	public List<Solution> eliminateDuplicates(int type, List<Solution> solutions){
		List<Solution> uniqueList = new ArrayList<Solution>();
		for (Solution s : solutions){
			boolean unique=true;
			for (Solution us:uniqueList){
				if (type==1&&s.equalsOther(us)){
					unique=false;
					break;
				}
				else if (type==2&&s.isRotationOf(us)){
					unique=false;
					break;
				}
				else if (type==3&&s.isRotationOrFlipOf(us)){
					unique=false;
					break;
				}
			}
			if (unique){
				uniqueList.add(s);
			}
			unique=true;
		}
		return uniqueList;	
	}
	
	/**
	 * ----------------------------ELIMINATE DUPLICATES MULTITHREADED  ------------------------------
	 */
	private List<Solution> multithreadEliminateDuplicates(int numthreads, int repetitions, int type, List<Solution> solutions){
		log("Starting multithread duplicate elimination. Type: "+type);
		long s1=System.nanoTime();
		List<Solution> uniques = solutions;
		for (int rep=0; rep<repetitions; rep++){
			long s2=System.nanoTime();
			int count = uniques.size();
			log("	Starting elimination round with "+count+" ids");
			int sizeofpartition = uniques.size()/numthreads;
			// create distribtion info
			int[] distributions = new int[numthreads*2+1];
			for (int pos=0; pos<(numthreads);pos++){
				distributions[pos*2]=sizeofpartition*(pos);
				distributions[pos*2+1]=sizeofpartition*(pos+1);
			}
			distributions[numthreads*2-1]=uniques.size();
			//create the machines
			List<NOminoComparisonMachine> machines = new ArrayList<NOminoComparisonMachine>();
			for (int t=0; t<numthreads; t++){
				NOminoComparisonMachine machine = new NOminoComparisonMachine(type, distributions[t*2], distributions[t*2+1], uniques, rep==repetitions);
				machines.add(machine);
			}
			//Start em up
			List<Thread> threads = new ArrayList<Thread>();
			for (NOminoComparisonMachine m:machines){
				Thread thread = new Thread(m);
				threads.add(thread);
				thread.start();
			}
			// Wait until the threads have finished
			boolean incomplete = true;
			while (incomplete){
				incomplete=false;
				for(Thread t : threads){
					if (t.isAlive()){
						incomplete=true;
					}
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			//Add results to Uniques list
			uniques = new ArrayList<Solution>();
			for(NOminoComparisonMachine m : machines){
				if (!m.isFinished()){
					System.out.println("MACHINE NOT FINISHED. ERROR");
				}
				uniques.addAll(m.getResults());
			}
			//uniques = shuffleArray(uniques);
			int diff = count-uniques.size();
			log("	Finished round. Removed "+diff+" items");
			log("	Round execution time: "+((double)(System.nanoTime()-s2)/(double)(1000000000)));
		}
		log("Clean beginning");
		long s3=System.nanoTime();
		int count=uniques.size();
		uniques=eliminateDuplicates(type,uniques);
		log("Clean time: "+((double)(System.nanoTime()-s3)/(double)(1000000000)));
		log("Clean removed: "+(count-uniques.size())+" items");
		log("Total time: "+((double)(System.nanoTime()-s1)/(double)(1000000000)));
		return uniques;
	}
}

