package me.jy89hui.main;

public class SpeedTester {
	
	/**
	 * RUNNING THIS PROGRAM WILL ONLY OUTPUT SAD TRUTHS.
	 * 
	 * DO NOT RUN THIS UNLESS YOU WANT TO BE SAD.
	 * 
	 * @param args
	 */
	public static void main(String[] args){
			int num = 200000;
			runBooleanTest(num);
			runByteTest(num);
			runIntegerTest(num);
	}
	public static void runBooleanTest(int items){
		long s1 = System.nanoTime();
		boolean[] vals = new boolean[items];
		for(int k=0; k<items; k++){
			vals[k]=(Math.random()>0.5);
		}
		long s2 = System.nanoTime();
		int truth=0;
		for(boolean b : vals){
			for(boolean b1:vals){
				if (b1==b){
					truth++;
				}
			}
		}
		long s3 = System.nanoTime();
		System.out.println("Boolean Test Complete.");
		System.out.println("Truth: "+truth);
		System.out.println("Init time: "+((double)(s2-s1)/(double)(1000000000)));
		System.out.println("Comp time: "+((double)(s3-s2)/(double)(1000000000)));
	}
	public static void runByteTest(int items){
		long s1 = System.nanoTime();
		byte[] vals = new byte[items];
		for(int k=0; k<items; k++){
			vals[k]=(byte) (Math.random()*128);
		}
		long s2 = System.nanoTime();
		int truth=0;
		for(byte b : vals){
			for(byte b1:vals){
				if (b1==b){
					truth++;
				}
			}
		}
		long s3 = System.nanoTime();
		System.out.println("Byte Test Complete.");
		System.out.println("Truth: "+truth);
		System.out.println("Init time: "+((double)(s2-s1)/(double)(1000000000)));
		System.out.println("Comp time: "+((double)(s3-s2)/(double)(1000000000)));
	}
	public static void runIntegerTest(int items){
		long s1 = System.nanoTime();
		int[] vals = new int[items];
		for(int k=0; k<items; k++){
			vals[k]=(int) (Math.random()*10000);
		}
		long s2 = System.nanoTime();
		int truth=0;
		for(int b : vals){
			for(int b1:vals){
				if (b1==b){
					truth++;
				}
			}
		}
		long s3 = System.nanoTime();
		System.out.println("Integer Test Complete.");
		System.out.println("Truth: "+truth);
		System.out.println("Init time: "+((double)(s2-s1)/(double)(1000000000)));
		System.out.println("Comp time: "+((double)(s3-s2)/(double)(1000000000)));
	}
}
