package me.jy89hui.main;

import java.util.List;

import me.jy89hui.dataStructures.Solution;

public class NOminoComparisonMachine implements Runnable{
	private boolean isFinished=false;
	private List<Solution> all;
	private List<Solution> results;
	private long timeTaken=0;
	private int type=0;
	private int start=0;
	private int end=0;
	private boolean lastround=false;
	/**
	 * 0 = NON REMOVED DUPLICATE LIST
	 * 1 = FIXED
	 * 2 = ONE SIDED
	 * 3 = FREE
	 */
	public NOminoComparisonMachine(int type, int start, int end, List<Solution> all, boolean lastRound) {
		this.all=all;
		this.type=type;
		this.lastround=lastRound;
		this.start=start;
		this.end=end;
	}
	@Override
	public void run() {
		isFinished=false;
		long s1=System.nanoTime();
		SolutionCreator sc = new SolutionCreator();
		List<Solution> startingSolutions = all.subList(start, end);
		if(this.lastround) {
			results=sc.eliminateDuplicatesComplicated(type, startingSolutions, start, all);
		}else {
			results=sc.eliminateDuplicates(type, startingSolutions);
		}
		timeTaken = System.nanoTime()-s1;
		isFinished=true;
	}
	public boolean isFinished(){
		return isFinished;
	}
	public List<Solution> getResults(){
		return results;
	}
	public long getTimeTaken(){
		return timeTaken;
	}
	public int getType(){
		return type;
	}
	

}
