package me.jy89hui.main;

public class UnitTests {
	static int[][] results={{0,1,2,6,19,63,216,760,2725,9910,36446,135268,505861},
			{0,1,1,2,7,18,60,196,704,2500,9189,33896,12759},
	{0,1,1,2,5,12,35,108,369,1285,4655,17073,63600}};
	static int order=9;
	static int thresh=7;
	public static void main(String[] args){
		if (args.length>0){
			order=Integer.parseInt(args[0]);
		}
		if (args.length>1){
			thresh=Integer.parseInt(args[1]);
		}
		runTests();
	}
	public static void runTests(){
		SolutionCreator sc = new SolutionCreator();
		boolean allPassed=true;
		for(int type=1; type<=3;type++){
			for (int x=1; x<=order; x++){
				long s1=System.nanoTime();
				int shouldbe=results[type-1][x];
				int createResult=sc.createOminos(x, type, false, thresh).size();
				boolean result=(shouldbe==createResult);
				String resultstr = result?"PASSED":"FAILED";
				System.out.println("Test, type: "+type+" order: "+x+" has "+resultstr+" in "+((double)(System.nanoTime()-s1)/(double)(1000000000))+" seconds");
				if(!result){
					System.out.println("	Should be: "+shouldbe+" Result gotten: "+createResult);
				}
				allPassed=result?allPassed:false;
			}
			System.out.println("--------------------------------------------");
		}
		System.out.println("Result: "+(allPassed?"SUCCESS":"FAILURE"));
	}
}
